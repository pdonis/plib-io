#!/usr/bin/env python
"""
Package PLIB -- Python Library
Copyright (C) 2008-2015 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This is the top-level namespace package for PLIB.
"""

from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
