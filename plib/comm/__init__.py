#!/usr/bin/env python
"""
Sub-Package COMM of Package PLIB -- "Child" Communication
Copyright (C) 2008-2015 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This sub-package contains utilities for working with and
managing and communicating with child threads and processes.
"""
